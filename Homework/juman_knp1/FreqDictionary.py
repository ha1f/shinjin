#coding: utf-8

class FreqDictionary(object):
    """
    頻度辞書のクラス

    update(key)
    get() -> Dictionary
    getItems() -> [(word: Object, freq: Int)]
    reset()
    """
    def __init__(self):
        self._dict = {}
    def update(self, key):
        """
        なければ作成、あればインクリメント
        """
        if key in self._dict:
            self._dict[key] += 1
        else:
            self._dict[key] = 1
    def get(self):
        return self._dict
    def getItems(self):
        """
        辞書を返す
        """
        return self._dict.items()
    def reset(self):
        """
        辞書を初期化
        """
        self._dict = {}
