#coding: utf-8

from pyknp import Juman

class JumanApi(object):
    """
    jumanのsingletonとして

    parse(string)
    """
    _juman = Juman()
    @staticmethod
    def parse(string):
        """
        stringを形態素解析し、list<mrph>として返す
        """
        res = JumanApi._juman.analysis(string)
        return res.mrph_list()
    @staticmethod
    def loadResultString(string):
        """
        一行分(〜EOS)の結果をlist<mrph>として返す
        """
        res = JumanApi._juman.result(string)
        return res.mrph_list()
    @staticmethod
    def loadJmn(lineIterator):
        """
        行ごとのイテレータを渡せば、list<list<mrph>>を返す
        """
        resList = []
        data = []
        for line in lineIterator: # 入力文を1行ずつ読む
            data.append(line)
            if line.strip() == "EOS": # 1文が終わったら解析
                result = JumanApi._juman.result("".join(data)).mrph_list()
                resList.append(result)
                data = []
        return resList
