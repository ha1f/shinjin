#coding: utf-8

# 標準ライブラリ
import sys
from itertools import chain

# 自作ライブラリ
from FreqDictionary import FreqDictionary
from JumanApi import JumanApi
from KnpApi import KnpApi

# MARK: Exercise

def ex61():
    """
    文を標準入力から読み込み、それを単語単位に分かち書きせよ (形態素間にスペースを挿入)
    """
    input_sentence = sys.stdin.readline()
    res = JumanApi.parse(input_sentence)
    midasis = [mrph.midasi for mrph in res]
    print(" ".join(midasis))

def ex62():
    """
    形態素解析結果を読み込み、名詞だけを抽出してプリントせよ
    """
    resLineList = JumanApi.loadJmn(iter(sys.stdin.readline, ""))
    for mrph in chain.from_iterable(resLineList):
        if mrph.hinsi == "名詞":
            print(mrph.midasi)

def ex63():
    """
    形態素解析結果を読み込み、動詞の基本形をプリントせよ
    """
    resLineList = JumanApi.loadJmn(iter(sys.stdin.readline, ""))
    for mrph in chain.from_iterable(resLineList):
        if mrph.hinsi == "動詞":
            print(mrph.genkei)

def ex64():
    """
    形態素解析結果を読み込み、形態素の原形を頻度順に並べよ
    """
    freqDict = FreqDictionary()
    resLineList = JumanApi.loadJmn(iter(sys.stdin.readline, ""))
    for mrph in chain.from_iterable(resLineList):
        freqDict.update(mrph.genkei)
    for k,v in sorted(freqDict.getItems(), key=lambda x: x[1], reverse=True):
        print(k)

def ex65():
    """
    形態素解析結果を読み込み、全形態素数 (総数) に対する述語の割合を計算せよ
    ここで、述語とは、動詞、イ形容詞 (形容詞)、ナ形容詞 (形容動詞) とする
    """
    resLineList = JumanApi.loadJmn(iter(sys.stdin.readline, ""))
    predicateCount = 0
    mrphCount = 0
    for mrph in chain.from_iterable(resLineList):
        if mrph.hinsi == "動詞" or mrph.hinsi == "形容詞":
            predicateCount += 1
        mrphCount += 1
    print("{}%".format(100 * predicateCount / mrphCount))

def ex66():
    """
    形態素解析結果を読み込み、「サ変名詞+する/できる」というパターンを抽出しプリントせよ
    """
    resLineList = JumanApi.loadJmn(iter(sys.stdin.readline, ""))
    # マッチするごとにリストに追加
    pattern = []
    for mrph in chain.from_iterable(resLineList):
        if len(pattern) > 0:
            if mrph.genkei == "する" or mrph.genkei == "できる":
                pattern.append(mrph.midasi)
                print("".join(pattern))
        if mrph.hinsi == "名詞" and mrph.bunrui == "サ変名詞":
            pattern = [mrph.midasi]
        else:
            pattern = []

def ex67():
    """
    形態素解析結果を読み込み、「AのB」という表現 (A と B は名詞の1形態素) をすべてプリントせよ
    """
    resLineList = JumanApi.loadJmn(iter(sys.stdin.readline, ""))
    matchers = [
        lambda mrph: mrph.hinsi == "名詞",
        lambda mrph: mrph.midasi == "の",
        lambda mrph: mrph.hinsi == "名詞",
    ]
    matchersCount = len(matchers)
    pattern = []
    for mrph in chain.from_iterable(resLineList):
        state = len(pattern)
        if matchers[state](mrph):
            pattern.append(mrph.midasi)
            if (state + 1) == matchersCount:
                matchedString = "".join(pattern)
                print(matchedString)
                pattern = []
        else:
            pattern = []

def ex68():
    """
    文を標準入力から読み込み、それを単語単位に分かち書きせよ (形態素間にスペースを挿入)
    """
    input_sentence = sys.stdin.readline()
    midasis = [mrph.midasi for mrph in KnpApi.parse(input_sentence).mrph_list()]
    print(" ".join(midasis))

def ex69():
    """
    構文解析結果を読み込み、接頭辞を含む文節をプリントせよ
    """
    res = KnpApi.loadKnp(iter(sys.stdin.readline, ""))
    for bnst in chain.from_iterable([line.bnst_list() for line in res]):
        if any(filter(lambda mrph: mrph.hinsi=="接頭辞", bnst.mrph_list())):
            midasi = "".join(mrph.midasi for mrph in bnst.mrph_list())
            print(midasi)

def ex70():
    """
    構文解析結果を読み込み、名詞を2つ以上含む文節をプリントせよ
    """
    res = KnpApi.loadKnp(iter(sys.stdin.readline, ""))
    for bnst in chain.from_iterable([line.bnst_list() for line in res]):
        if len(list(filter(lambda mrph: mrph.hinsi=="名詞", bnst.mrph_list()))) >= 2:
            midasi = "".join(mrph.midasi for mrph in bnst.mrph_list())
            print(midasi)

#必要な関数以外をコメントアウトして実行する

"""
ex61
Homeworkフォルダにて、
python juman_knp_1/main.py
を実行
続けて標準入力に入力する
"""
#ex61()

"""
ex62-67
Homeworkフォルダにて、
python juman_knp1/main.py < resource/200501a.jmn
を実行
"""

#ex62()
#ex63()
#ex64()
#ex65()
#ex66()
ex67()

"""
ex68
Homeworkフォルダにて、
python juman_knp1/main.py
を実行
続けて標準入力に入力する
"""
#ex68()

"""
ex69-70
Homeworkフォルダにて、
python juman_knp1/main.py < resource/200501a.knp
を実行
"""
#ex69()
#ex70()
