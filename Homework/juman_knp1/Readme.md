# pyknp

## model
#### pyknp.juman.mlist.MList
.mrph_list()
    -> list<pyknp.juman.morpheme.Morpheme>

#### pyknp.knp.blist.BList
.bnst_list()
    文節のリスト
.tag_list()
    基本句のリスト
.mrph_list()
    形態素のリスト

#### pyknp.juman.morpheme.Morpheme
.midasi
.yomi
.genkei
.hinsi
.bunrui
.katuyou1
.katuyou2
.imis
.repname
    代表表記

#### pyknp.knp.bunsetsu.Bunsetsu
.mrph_list()
見出しは、

```python
"".join(mrph.midasi for mrph in bnst.mrph_list())
```

として取得


#### Juman
.analysis("今日はいい天気ですね")
    -> pyknp.juman.mlist.MList
.result()

#### Knp
.parse()
.result()

#### 文節
for bnst in result.bnst_list():
        print("文節ID:", bnst.bnst_id)
        print("見出し:", "".join(mrph.midasi for mrph in bnst.mrph_list()))
        print("係り受けタイプ:", bnst.dpndtype)
        print("親文節ID:", bnst.parent_id)
        print("正規化代表表記:", bnst.repname)
        print("素性:", bnst.fstring)
        print("--------------------")

#### 基本句
for tag in result.tag_list():
        print("基本句ID:", tag.tag_id)
        print("見出し:", "".join(mrph.midasi for mrph in tag.mrph_list()))
        print("係り受けタイプ:", tag.dpndtype)
        print("親基本句ID:", tag.parent_id)
        print("正規化代表表記:", tag.repname)
        print("素性:", tag.fstring)

#### 形態素
for mrph in result.mrph_list():
        print("見出し:", mrph.midasi)
        print("読み:", mrph.yomi)
        print("原形:", mrph.genkei)
        print("品詞:", mrph.hinsi)
        print("品詞細分類:", mrph.bunrui)
        print("活用型:", mrph.katuyou1)
        print("活用形:", mrph.katuyou2)
        print("意味情報:", mrph.imis)
        print("代表表記:", mrph.repname)
