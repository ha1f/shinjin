#coding: utf-8

import sys


hw1 = lambda a,b,c: a if a > b and a > c else b if b > c else c

# @return (index, maxValue)
def hw2(l):
    return max(enumerate(l), key=lambda x: x[1])

def hw3(l):
    res = [(v if i==0 else 0)  for i,v in enumerate(l)]
    return res

def hw4_1(n1, n2):
    nextValue = n1+n2
    if nextValue > 1000:
        return False
    print(nextValue, end=" ")
    return hw4_1(n2, nextValue)

def hw4_2(n1, n2):
    print(n1, n2, end=" ")
    nextValue = n1+n2
    while nextValue < 1000:
        print(nextValue, end=" ")
        n1 = n2
        n2 = nextValue
        nextValue = n1+n2
    return

def swap(l, i, j):
    tmp = l[j]
    l[j] = l[i]
    l[i] = tmp
    return

def quickSort(l):
    ints = l[:]
    pivot = ints[0]
    left = 0
    right = len(ints)
    if right == 1:
        return ints
    while left < right:
        for index,v in enumerate(ints[left:]):
            if v >= pivot:
                left = index + left
                break
        for index,v in enumerate(reversed(ints[left-1:right])):
            if v < pivot:
                right = right - index - 1
                break
        else:
            left += 1
            right = index
        if left < right:
            swap(ints, left, right)
            left += 1
    return quickSort(ints[:left]) + quickSort(ints[left:])

def hw5(ints):
    return quickSort(ints)

# TODO: use itrtools
def createFreqDict(data):
    data = ['eggs', 'spam', 'spam', 'bacon']
    res = {}
    for d in data:
        if d not in res.keys():
            res[d] = 1
        else:
            res[d] += 1
    return res

def ex10():
    data = ['eggs', 'spam', 'spam', 'bacon']
    res = createFreqDict(data)
    print(sorted(res.items(), key=lambda x: x[1], reverse = True))
    print()

# MARK: hw1
print(hw1(2,3,5))

# MARK: hw2
print(hw2([3, 5, 2, 4, 2]))

# MARK: hw3
print(hw3([3, 5, 2, 4, 2]))

# MARK: hw4_1
print(1, 1, end=" ")
hw4_1(1, 1)
print()

# MARK: hw4_2
hw4_2(1, 1)
print()

# MARK: hw4_3
# 再帰を用いないメリット
#    終了条件を考えやすい
#    手続きが明確で、printしやすい
# 再帰を用いるメリット
#    コードが短くなる

# MARK: hw5
print(hw5([3,2,0,5,8,3,4,1,3,2, -1, 0, 4]))
