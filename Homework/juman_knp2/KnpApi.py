#coding: utf-8

from pyknp import KNP

class KnpApi:
    """
    knpのsingletonとして

    parse(string)
    """
    _knp = KNP()
    @staticmethod
    def parse(string):
        """
        stringを解析し、list<blist>として返す
        """
        res = KnpApi._knp.parse(string)
        return res
    @staticmethod
    def loadResultString(string):
        """
        一行分(〜EOS)の結果をlist<blist>として返す
        """
        res = KnpApi._knp.result(string)
        return res
    @staticmethod
    def loadKnp(lineIterator):
        """
        行ごとのイテレータを渡せば、list<list<blist>>を返す
        """
        resList = []
        data = []
        for line in lineIterator: # 入力文を1行ずつ読む
            data.append(line)
            if line.strip() == "EOS": # 1文が終わったら解析
                result = KnpApi._knp.result("".join(data))
                resList.append(result)
                data = []
        return resList
