#coding: utf-8

# This code is homework

from functools import reduce
import math

def closedRange(start, stop, step=1):
  direction = 1 if (step > 0) else -1
  return range(start, stop + direction, step)

def ex1():
    print("Tomoki Yamaguchi")
# ex2 is to add comment
def ex3():
    firstName = "Tomoki"
    lastName = "Yamaguchi"
    print(" ".join([firstName, lastName]))
def ex4():
    res = 5**4
    print(res)
def ex5():
    res = 1/4
    print(res)
def ex6():
    try:
        print(1/0)
    except ZeroDivisionError:
        print("Error: ZeroDivisionError occured")

def hw1():
    for i in closedRange(1, 30):
        print(i, end=", " if i<30 else "\n")
def hw2():
    for i in filter(lambda x: x%2==0, closedRange(1, 30)):
        print(i, end=", " if i<30 else "\n")
def hw3():
    res = sum(closedRange(1, 30))
    print(res)
def hw4():
    res = reduce(lambda a, b: a*b, closedRange(1, 10))
    print(res)
def hw5():
    def showTable(table):
        for line in table:
            print(" ".join(map(lambda i: "{0:>2}".format(i), line)))
    table = [[i*j for i in closedRange(1, 9)] for j in closedRange(1, 9)]
    showTable(table)
def hw6():
    def formatFizzBuzz(x):
        if x%15 == 0:
            return "Fizz Buzz"
        elif x%3 == 0:
            return "Fizz"
        elif x%5 == 0:
            return "Buzz"
        return str(x)
    for i in closedRange(1, 100):
        print(formatFizzBuzz(i))
def hw7():
    # deprecated and unused
    def isPrime(x):
        if x < 2:
            return False
        if x == 2:
            return True
        if any(filter(lambda i: x%i==0, closedRange(2, int(math.sqrt(x))))):
            return False
        return True
    primes = []
    nums = list(closedRange(2, 1000))
    while len(num) > 0:
        i = nums[0]
        primes.append(i)
        nums = list(filter(lambda x: x%i != 0, nums))
    print(primes)

ex1()
ex3()
ex4()
ex5()
ex6()
hw1()
hw2()
hw3()
hw4()
hw5()
hw6()
hw7()
