#coding: utf-8

import math
import re

# MARK: EXERCISE

path_to_resource = "../resource/"

def ex1():
    """
    print area of circle with the radius of 3.5, with format {0:.2f}
    """
    calcAreaOfCircle = lambda r: pow(r, 2) * math.pi
    radius = 3.5
    s = calcAreaOfCircle(radius)
    print("{0:.2f}".format(s))

def ex2_1():
    with open("a_copy.txt", 'w') as f2:
        with open(path_to_resource + "a.txt") as f:
            for line in f:
                f2.write(line + "\n")

def ex2_2():
    with open("test.txt", 'bw') as f2:
        with open(path_to_resource + "a.txt") as f:
            for line in f:
                s = " ".join(list(line)) + "\n"
                f2.write(s.encode('utf-8'))

def ex3_1_a(s):
    """
    check if a string is empty without a regex

    Args:
        s: target string data
    Returns:
        Boolean value of if a string is empty or not
    """
    if s:
        return False
    else:
        return True

def ex3_1_b(s):
    """
    check if a string is empty with regex

    Args:
        s: target string data
    Returns:
        Boolean value if a string is empty or not
    """
    if re.match(r"^$", s):
        if re.match(r"^\n$", s):
            return False
        else:
            return True
    else:
        return False

def ex3_2_a(s):
    """
    Double all vowels in some input string without a regex

    Args:
        s: input string data
    Returns:
        transformed string data
    """
    vowels = list("aeiouAEIOU")
    charcters = []
    for c in s:
        if c in vowels:
            charcters.extend([c, c])
        else:
            charcters.append(c)
    return "".join(charcters)

def ex3_2_b(s):
    """
    Double all vowels in some input string with a regex

    Args:
        s: input string data
    Returns:
        transformed string data
    """
    return re.sub(r"([aeiouAEIOU])", r"\1\1", s)

# MARK: EXERCISE TEST

def test_ex3_1():
    assert ex3_1_a("") == True
    assert ex3_1_a(" ") == False
    assert ex3_1_a("fdsafjdas") == False
    assert ex3_1_a("\n") == False
    assert ex3_1_a("\t") == False
    assert ex3_1_b("") == True
    assert ex3_1_b(" ") == False
    assert ex3_1_b("fdsafjdas") == False
    assert ex3_1_b("\n") == False
    assert ex3_1_b("\t") == False

def tesst_ex3_2():
    assert ex3_2_a("Sir Robin") == "Siir Roobiin"
    assert ex3_2_b("Sir Robin") == "Siir Roobiin"

# MARK: HOMEWORK

def hw1():
    freq_dict = {}
    def update_freq_dict(s):
        """register or increment freq_dict"""
        if s in freq_dict:
            freq_dict[s] += 1
        else:
            freq_dict[s] = 1
    def calc_p_a(s1):
        """calc P(a)"""
        return freq_dict[s1] / len(freq_dict)
    def calc_p_b_given_a(s1, s2):
        """calc P(b|a)"""
        return freq_dict["%s %s" % (s1,s2)] / freq_dict[s1]
    def calc_p_a_precedes_b(s1, s2):
        """calc P(a b)"""
        p_a = calc_p_a(s1)
        p_b_given_a = calc_p_b_given_a(s1, s2)
        return p_a * p_b_given_a
    def build_freq_dict():
        """build unigram+bigram freq_dict from file"""
        with open(path_to_resource + "doc0000000000.txt") as f:
            is_invalid_line = lambda l: re.match(r"<PAGE URL=", l) or re.match(r"</PAGE>", l)
            pre = ""
            for line in f:
                if is_invalid_line(line):
                    continue
                for w in line.split():
                    unigram = w.strip().strip(".,").lower()
                    update_freq_dict(unigram)
                    if pre:
                        bigram = "%s %s" % (pre, unigram)
                        update_freq_dict(bigram)
                    pre = unigram
    def calc_probability(sentence):
        """estimate the probability of the sentence"""
        pre = ""
        p = 1.0
        for w in sentence.split():
            word = w.strip().strip(".,").lower()
            if pre:
                p *= calc_p_a_precedes_b(pre, word)
            pre = word
        return p
    build_freq_dict()
    res = calc_probability("The man is in the house.")
    print(res)

ex1()
ex2_1()
ex2_2()
test_ex3_1()
tesst_ex3_2()

#hw1()
# result: 1.2542566224086207e-18
