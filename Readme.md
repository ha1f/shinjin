# 新人研修用フォルダ

## Author
Tomoki Yamaguchi: [@_ha1f](http://twitter.com/_ha1f)

## 環境
Python 3.5.1 :: Anaconda 4.0.0 (x86_64)

## ファイル構成
####  Homework/*

宿題用

juman_knp1の実行は問題番号ごとに三種類ある。

```bash
cd Homework
python juman_knp1/main.py < resource/200501a.jmn
```

```bash
cd Homework
python juman_knp1/main.py < resource/200501a.knp
```

```bash
cd Homework
python juman_knp1/main.py
```

のうちのいずれか


#### nlp100/*
100本ノック練習用

## homework resource
https://bitbucket.org/ku_nlp/shinjin/src
