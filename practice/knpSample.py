#coding: utf-8

from pyknp import KNP
import sys

knp = KNP()

string = input()
result = knp.parse(string)

for bnst in result.bnst_list():
    print("".join(mrph.midasi for mrph in bnst.mrph_list()))



