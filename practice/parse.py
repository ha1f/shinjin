#coding: utf-8

from pyknp import Juman
import sys

juman = Juman()

data = ""
for line in iter(sys.stdin.readline, ""):
	data += line
	if line.strip() == "EOS":
		result = juman.result(data)
		print(",".join(mrph.midasi for mrph in filter(lambda mrph: mrph.hinsi=="名詞", result.mrph_list())))
		data = ""

