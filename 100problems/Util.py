#coding: utf-8
import random

class Util:
    nGram = lambda l, n: [[l[i] for i in range(j, j+n)] for j in range(0, len(l)-n+1)]
    nGramWord       = lambda s, n: Util.nGram(s.split(" "), n)
    nGramCharacter  = lambda s, n: [s1+s2 for s1, s2 in Util.nGram(s.replace(" ", ""), n)]
    reversedString = lambda s: s[::-1]
    def shuffledList(s):
        shuffledList = list(s)
        random.shuffle(shuffledList)
        return shuffledList
