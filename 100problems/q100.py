#coding: utf-8

from Util import *
import random

q0 = Util.reversedString
q1 = lambda s: s[::2]
q2 = lambda s1, s2: "".join(map(lambda ss: ss[0]+ss[1], zip(s1, s2)))
q3 = lambda s: [ss[0] for ss in s.split(" ")]
def q4(s):
    oneCharIds = (1, 5, 6, 7, 8, 9, 15, 16, 19)
    return {index+1:rawName[0:(1 if (index+1) in oneCharIds else 2)] for index, rawName in enumerate(s.split(" "))}
def q5(s):
    wordRes = Util.nGramWord(s, 2)
    charRes = Util.nGramCharacter(s, 2)
    return (wordRes, charRes)
def q6():
    x = set(Util.nGramCharacter("paraparaparadise", 2))
    y = set(Util.nGramCharacter("paragraph", 2))
    unionSet = x | y
    productSet = x & y
    subSet = x ^ y
    isXContainSE = "se" in x
    isYContainSE = "se" in y
    return (unionSet, productSet, subSet, isXContainSE, isYContainSE)
q7 = lambda x,y,z: str(x) + "時の" + str(y) + "は" + str(z)
cipher = lambda s: "".join(map(lambda c: chr(219-ord(c)) if c.islower() else c, s))
q8 = cipher
def q9(s):
    words = s.split(" ")
    formatter = lambda word: word[0] + "".join(Util.shuffledList(word[1:-1])) + word[-1] if len(word) > 4 else word
    formattedWords = map(formatter, words)
    return " ".join(formattedWords)

def assertEqualAndPrint(f, resource, ans):
    res = f(*resource) if isinstance(resource, tuple) else f(resource)
    print(res, end="...")
    if res == ans:
        print("Succeeded")
        return True
    else:
        print("Failed")
        return

def main():
    assertEqualAndPrint(q0, "stressed", "desserts")
    assertEqualAndPrint(q1, "パタトクカシーー", "パトカー")
    assertEqualAndPrint(q2, ("パトカー", "タクシー"), "パタトクカシーー")
    assertEqualAndPrint(q3, "Now I need a drink, alcoholic of course, after the heavy lectures involving quantum mechanics.", list("NInadaocathliqm"))
    assertEqualAndPrint(q4, "Hi He Lied Because Boron Could Not Oxidize Fluorine. New Nations Might Also Sign Peace Security Clause. Arthur King Can.", {1: 'H', 2: 'He', 3: 'Li', 4: 'Be', 5: 'B', 6: 'C', 7: 'N', 8: 'O', 9: 'F', 10: 'Ne', 11: 'Na', 12: 'Mi', 13: 'Al', 14: 'Si', 15: 'P', 16: 'S', 17: 'Cl', 18: 'Ar', 19: 'K', 20: 'Ca'})
    assertEqualAndPrint(q5, "I am an NLPer", ([['I', 'am'], ['am', 'an'], ['an', 'NLPer']], ['Ia', 'am', 'ma', 'an', 'nN', 'NL', 'LP', 'Pe', 'er']))
    assertEqualAndPrint(q6, (), ({'ar', 'ra', 'se', 'gr', 'di', 'ag', 'ad', 'ph', 'pa', 'is', 'ap'}, {'ar', 'pa', 'ra', 'ap'}, {'se', 'gr', 'is', 'di', 'ag', 'ad', 'ph'}, True, False))
    assertEqualAndPrint(q7, (12, "気温", 22.4), "12時の気温は22.4")
    assertEqualAndPrint(q8, q8("you are the hero"), "you are the hero")
    print(q9("I couldn't believe that I could actually understand what I was reading : the phenomenal power of the human mind ."))

if __name__ == "__main__":
    main()
